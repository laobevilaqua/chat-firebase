// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDwI5AUfVnb9vPjEAY6g7SI4CEQmnuHTac",
    authDomain: "chat-web-8bb8d.firebaseapp.com",
    databaseURL: "https://chat-web-8bb8d.firebaseio.com",
    projectId: "chat-web-8bb8d",
    storageBucket: "chat-web-8bb8d.appspot.com",
    messagingSenderId: "244700826867",
    appId: "1:244700826867:web:dc742e4fda01079c"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
