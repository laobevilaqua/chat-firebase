import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from "firebase";
import * as moment from 'moment';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  public chatId = '';
  public chat$;
  public newMessage = '';
  public userId = Math.random().toString(16).substr(2,18);
  public userName = ''
  public array = [];

  constructor(private route: ActivatedRoute, private firestore: AngularFirestore) { }

  ngOnInit() {
    this.chatId = this.route.snapshot.paramMap.get('id').toString();
    this.chat$ = this.firestore.collection('chats-teste').doc(this.chatId).valueChanges();
    this.userName = localStorage.getItem('userName') || 'Anônimo';
    this.chat$.subscribe(response => {
      response.message.forEach(element => {
        this.userFilter(element)
      });
    });
  }

  sendMessage() {
    if(this.newMessage != '') {
      this.firestore.collection('chats-teste').doc(this.chatId).update({
        message: firebase.firestore.FieldValue.arrayUnion({
          text: this.newMessage,
          date: moment().format('DD/MM/YYYY HH:mm'),
          userName: this.userName,
          userId: this.userId
        })
      }).then(response => {
        this.newMessage = '';
      });
    }
  }

  userFilter(user) {
    console.log(user)
      this.array.push({ id: user.userId, name: user.userName, text: user.text});
      this.array = this.array.filter((test, index, array) =>
        index === array.findIndex((findTest) =>
          findTest.name === test.name
        )
      );
  }

}
