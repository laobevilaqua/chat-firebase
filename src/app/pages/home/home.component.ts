import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from "firebase";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public userName = '';
  public chatid = '';
  public show = false;
  public newChatId = ''
  constructor(private router: Router, private firestore: AngularFirestore) { }

  ngOnInit() {
    this.userName = localStorage.getItem('userName');
  }

  singIn() {
    localStorage.setItem('userName', this.userName);
    this.router.navigate(['chat/'+this.chatid.toUpperCase()] );
  }
  
  CreateRoom() {
    this.firestore.collection("chats-teste").doc(this.newChatId).set({ message:[{
      text: '',
      userName: '',
      date: '',
      userId: ''
    }]}).then(response =>
        this.changeShow()
      )
    }
  
    changeShow(): void {
      this.show = !this.show;
  }
}
